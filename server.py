import socket
host = "angsila.informatics.buu.ac.th"
port = 12345
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.bind((host, port))
server_socket.listen()
connection, client_address = server_socket.accept()
with open('a.txt', 'rb') as file:
    data = file.read(1024)
    while data:
        connection.send(data)
        data = file.read(1024)
connection.close()
print("closed")